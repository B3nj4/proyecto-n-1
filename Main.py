import os
import Matrix 
import Intera

def main():
                
    matriz01 = Matrix.crea_matriz()
    
    Intera.posicion_inicial(matriz01)
    print("\n")
    Matrix.muestra_casillas(matriz01)
            
    while True:       
        
        Matrix.muestra_casillas(matriz01)
        Intera.interaccion_matriz(matriz01)
        os.system("clear")
            
        if matriz01[9][9] == "@":
            print("\n////// Ha llegado a la meta \\\\\\\\\\\\\\ \n")
            #print("//////La cantidad de movimientos fue de: ",cont," saltos hasta llegar a la meta\\\\\\")
            
            print("---> Para reiniciar ejecute nuevamente el fichero Main.py   ")
            print("---> Para finalizar no es necesario hacer nada .... el programa lo hacer solo   \n")
            break
                                                       
              
main()