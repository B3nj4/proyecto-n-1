# Proyecto N°1 

Proyecto N°1 
Programacion I, Python 

Por: Benjamin Vera Garrido - Paz Echeverria

¿Como correr el programa?
Para poder interactuar con el programa es necesario tener las funciones "Main.py", "Intera.py" y "Matrix.py"; teniendo esas solo es necesario poder ejecutar el fichero "Main.py" en un IDE o editor de texto con Python 3.8 funcionable (es importante destacar que los otros ficheros en la misma carpeta que "Main.py") 

¿En que consiste el proyecto?
El proyecto consiste en recorrer un camino que llega desde un punto de partida ubicado en la esquina superior izquierda del mapa hasta llegar a la meta ubicada en la esquina inferior derecha.
Para ello debera analizar el camino aleatorio que se le otorgara el cual estara delimitado por "x" y seguirlo para llegar a la meta; pero cuidado que el camino tendra muchas variantes y caminos falsos por lo que llegar a la meta podria no resultar tan facil como es de esperarse. Es importante aclarar tambien, que habran espacios que no pueden recorrerse, estos estan represantados por el signo "·"

¿Como puedo moverme?
El movimiento por el camino sera utilizando las teclas "A" para moverse hacia la izquierda, "D" para moverse hacia la derecha, "W" para moverse hacia arriba y "S" hacia abajo. Procurando seguir el camino de "x"
 
¿Cuando termina el juego?
El juego termina cuando se haya llegado a la meta. El usuario decide si reiniciar el mapa para continuar jugando o finalizar la partida

¿Como se ha codificado?
El juego consta de funciones que conformaran parte de la funcion principal "Main.py" que se encargara de imprimir y correr el desafio entre esas funciones podemos encontrar:

-"Matrix.py": Funcion que se encargaria de crear el camino del desafio para poder recorrerlo. Consiste en crear una matriz de tamaño 10x10 mas tres casos en los que se creara un camino predeterminado que llega desde el punto de partida hasta la meta. La forma en que se haran las ramificaciones y caminos falsos es muy simple, consiste de crear un numero aleatorio entre el 0 y el 2, donde, dependiendo el caso/composicion del camino que se haya asignado (caso 1 = 0, caso 2 = 1, caso 3 = 2), aparecera en pantalla. Ademas, estos casos tienen en el otra funcion aleatoria donde pondran un valor "x" ;creacion de otros caminos, en cualquier posicion de la matriz, generando asi, algunas ramificaciones en el camino principal para que este tenga cierta dificultad (las ramificaciones tienen diferentes aleatoriedades y frecuancia dependiendo el caso, por ejemplo, si el caso tiene bastante camino por recorrer, los "x" apareceran en menor cantidad que en otro camino con poco lugar para llegar a la meta).Todo lo anterior se encuentra en la sub funcion "crea _matriz()"
Otras sub funciones que tiene "Matrix.py" es la presencia de "impresion_matrix(matriz)" donde -como su nombre lo indica- hace impresion la matriz, por otro lado, la otra sub funcion presente en "Matrix.py" es "muestra _casillas(matriz)" en el cual, presenta mejoras graficas de la impresion de la matriz (como agregar un contorno a la impresion y separacion decente entre cada elemento de la matriz, es decir, "x" que es al camino por recorrer y "·" que representaria el camino no recorrible)

-"Intera.py": Funcion que contendra toda la informacion y procesos relacionados con la interaccion matriz-teclado, en esta funcion se nos presentaran las sub funciones "posicion_inicial(matriz)" -encargada definir la posicion en la que comenzaremos,esta es, la posicion de la matriz "[0][0]"-, "interaccion_matriz(matriz)" que se encargara de dar las condiciones y acciones que se ejecutaran dependiendo del ingreso de "A,D,S,W" y retornando los cambios en la matriz,para ello, una variable "i" y "j" recorreran la matriz con los caminos aleatorios ya impresos y ubicando la posicion donde esta parado el usuario (en "@") se pondran condiciones del movimiento. 


  ////////// Algunos de muchos errores \\\\\\\\\\\

- El contador de movimiento salio defectuoso, no considera la variable "cont" destinada a ello
- No hay un menu decente para este gran proyecto
- El codigo no es muy prolijo y hay algunas variables bastante implicitas con respecto a su contenido-nombre
- Proliferan llamados de funciones exageradamente
- La meta y el inicio no tuvieron u distintivo de las demas, por lo que, llegar a la meta es bastante monotono


» Gracias por leer 

