import random
#creación de la matriz
matriz =  [["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"],
           ["·", "·", "·", "·", "·", "·", "·", "·", "·", "·"]]

def crea_matriz():
    
    #/////////////"aleatoriedad" del camino/////////////////
    recurrencia = random.randint(0,2)

    #caso_1
    if recurrencia == 0:
        for fila in range (10):
            for columna in range (10):
                xd = random.randint(0,3)
                if xd == 1 or xd == 2 :
                    matriz[fila][columna] = "x"
                if (fila == 0 and -1 < columna < 10) or (-1 < fila < 10 and columna == 9):
                    matriz[fila][columna] = "x"
                    matriz[0][0] = "x"   

    #caso_2
    elif recurrencia == 1:
        for fila in range (10):
            for columna in range (10):
                xd = random.randint(0,3)
                if xd == 1:
                    matriz[fila][columna] = "x"
                if (fila == 0 and -1 < columna < 6) or (-1 < fila < 6 and columna == 5) or ( fila == 5 and 0 < columna < 6 ) or (4 < fila < 8 and columna == 1) or (fila == 7 and 0 < columna < 5) or (6 < fila < 10 and columna == 4) or (fila == 9 and 3 < columna < 10) :
                    matriz[fila][columna] = "x" 

    #caso_3
    elif recurrencia == 2:    
        for fila in range (10):
            for columna in range (10):
                xd = random.randint(0,3)
                if xd == 1 or xd == 2 :
                    matriz[fila][columna] = "x"

                if (columna == 0 and -1 < fila < 10) or (-1 < columna < 10 and fila == 9):
                    matriz[fila][columna] = "x"
                    matriz[0][0] = "x" 
    
    return matriz
        
def impresion_matrix(matriz):
    print("\n")
    for fila in matriz:
       print(fila)
    
def muestra_casillas(matriz):
    # Procedimiento
    secuencia = 1
    for fila in matriz:
        texto = "| "
        for casilla in fila:
            texto = texto + casilla + " "
             
        print(texto+"|")
    
    print("\n")   

    



